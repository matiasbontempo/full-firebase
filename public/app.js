AppendMsg("Full Firebase");

// Initialize Firebase
firebase.initializeApp({
	apiKey: "AIzaSyBRHEg8gePKt79tTVgWfSPN5RLe19uddHU",
	authDomain: "full-firebase.firebaseapp.com",
	databaseURL: "https://full-firebase.firebaseio.com",
	projectId: "full-firebase",
	storageBucket: "full-firebase.appspot.com",
	messagingSenderId: "689030704100"
});

const messaging = firebase.messaging();
messaging.requestPermission()
	.then(() => {
		AppendMsg("Permission granted");
		return messaging.getToken();
	})
	.then(token => {
		const _token = localStorage.getItem("token");
		if (_token) firebase.database().ref("users/"+_token).remove();
		firebase.database().ref("users/"+token).set(true);
		localStorage.setItem("token", token);
		AppendMsg(token);
	})
	.catch(err => {
		AppendMsg("Error occurred: " + JSON.stringify(err));
	});

messaging.onTokenRefresh()
	.then(() => {
		AppendMsg("Token Refresh");
		return messaging.getToken();
	})
	.then(token => {
		const _token = localStorage.getItem("token");
		if (_token) firebase.database().ref("users/"+_token).remove();
		firebase.database().ref("users/"+token).set(true);
		localStorage.setItem("token", token);
		AppendMsg(token);
	})
	.catch(err => {
		AppendMsg("Error occurred: " + JSON.stringify(err));
	});

messaging.onMessage(payload => {
	console.log("onMessage: ", payload);
	AppendMsg(JSON.stringify(payload));
});


function AppendMsg(msg) {
	const div = document.createElement("div");
	div.appendChild(document.createTextNode(msg));
	document.body.appendChild(div);
}