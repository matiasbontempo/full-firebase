const functions = require('firebase-functions');
const admin = require('firebase-admin');
const serviceAccount = require("./full-firebase-firebase-adminsdk-pgerj-308677fbff.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://full-firebase.firebaseio.com"
});

exports.sendPush = functions.database.ref('/notification').onWrite(event => {

	const notif = event.data.val();
	if (notif.sent) return;

	console.log("New notification to send!");

	const payload = {
		notification: {
			text: notif.text,
			title: notif.title,
			click_action: notif.click_action,
			icon: "./icons/mstile-144x144.png"
		}
	}

	admin.database().ref("/users").once("value")
		.then(snapshot => {
			Object.keys(snapshot.val()).map((token) => {
				console.log("Sending notification to: "+token);
				admin.messaging().sendToDevice(token, payload)
					.catch(error => console.error("Error sending message to "+token+":", error));
			});
		})
		.catch(error => {
			console.log("Error sending message:", error);
		});

	event.data.adminRef.update({ sent: true });
});