importScripts('https://www.gstatic.com/firebasejs/4.1.3/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.1.3/firebase-messaging.js');

// Initialize Firebase
firebase.initializeApp({
	apiKey: "AIzaSyBRHEg8gePKt79tTVgWfSPN5RLe19uddHU",
	authDomain: "full-firebase.firebaseapp.com",
	databaseURL: "https://full-firebase.firebaseio.com",
	projectId: "full-firebase",
	storageBucket: "full-firebase.appspot.com",
	messagingSenderId: "689030704100"
});

const messaging = firebase.messaging();